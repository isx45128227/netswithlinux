CONFIGURAR UN ROUTER
1. poner IPs
2. Routing
3. NAT (REDIRECCIÓN DE PUERTOS Y MASQUERADE)
4. Filtrado
5. QoS


```


           internet                                         ((-+
        +---------------------------+                          |
                      |DYN_IP_address          +-))            |.101
                      |inet                    |            +-------+
                  +--------+                   |            |tabletA|
                  | Router |eth1  10.0.0.0/24  |.100        +-------+
                  | ISP    +---------------------+
                  |        |.1       |.10     |.11         ((-+
                  +--------+         |        |               |
                      |eth2      +------+ +-------            |.102
   10.10.2.0/24       |.1        |host A| |host B|         +-------+
  --------------------------     +------+ +------+         |tabletB|
           |.10                                            +-------+
           |
    +-------------+
    |server       |
    |www          |
    |ports 80,443 |
    +-------------+

```
CONFIGURAR UN ROUTER

1. poner IPs

```
ip a a 10.0.0.1/24 dev eth1
ip a a 10.10.2.1/24 dev eth2
dhclient inet
```

2. Routing

```
#En este caso, como no hay más redes y el default gateway me lo dan por dhcp
#no hace falta configurar nada más, excepto el bit de forwarding
 echo 1 > /proc/sys/net/ipv4/ip_forward
```

3. NAT (REDIRECCIÓN DE PUERTOS Y MASQUERADE)

```
#MASQUERADE
iptables -t nat -A POSTROUTING -o inet -j MASQUERADE

#DNAT

iptables -t nat -A PREROUTING -i inet -p tcp --dport 80  -j DNAT --to 10.10.2.10:80
iptables -t nat -A PREROUTING -i inet -p tcp --dport 443  -j DNAT --to 10.10.2.10:443
```

4. Filtrado

```
#REDUCIR LAS TABLAS
#con supernetting:
    
iptables -t filter -A FORWARD  -d 10.10.2.10  -s 10.0.0.100/30 -p tcp --dport 80 -j ACCEPT 
iptables -t filter -A FORWARD  -d 10.10.2.10  -s 10.0.0.100/30 -p tcp --dport 443 -j ACCEPT 
iptables -t filter -A FORWARD  -s 10.0.0.100/30 -o eth2 -j DROP

#con módulos iprange y multiport
iptables -t filter -A FORWARD  -d 10.10.2.10  \
                               -p tcp 
                               -m iprange --src-range 10.0.0.101-10.0.0.102 \
                               -m multiport --destination-ports 80,443 \
                               -j ACCEPT 
                               
iptables -t filter -A FORWARD  -m iprange --src-range 10.0.0.101-10.0.0.102 \
                               -o eth2 \
                               -j DROP
```

5. QoS


Buscar un modulo de iptables que permita filtrar rangos IP
```
iptables -m iprange -–src-range IP1-IP2 -j ACTION (Direccion origen)
                 --dst-range IP1-IP2 -j ACTION (Direccion Destino)
```
                                       
Buscar un modulo de iptables que permita filtrar por MAC

```
iptables -m mac --mac-source aa:aa:aa:aa:aa:aa -j ACTION
```

Buscar un modulo de iptables que permita filtrar por franja horaria
```
iptables -m time --timestart HORA1 --timestop HORA2 -j ACTION (Formato 24h HH:MM)
```

Buscar un módulo para permitir rangos de puertos en el filtrado, en una sóla regla varios puertos

```

```
-------------------------------------------------------

#EJERCICIO

<<<<<<< HEAD
Redireccionar cualquier consulta dns que vaya a 8.8.8.8 al servidor DNS 10.1.1.200

	iptables -t nat -A PREROUTING -o eth0 -d 8.8.8.8 -j DNAT --to 10.1.1.200


Redireccionar los puertos 8000 al 8005 a las ips 192.168.111.10-192.168.111.15

	iptables -t nat -A PREROUTING -p tcp -m multiport -sports 8000:8005 -j DNAT --to 192.168.111.10-192.168.111.15

=======
## Redireccionar cualquier consulta dns que vaya a 8.8.8.8 al servidor DNS 10.1.1.200

## Redireccionar los puertos 8000 al 8005 a las ips 192.168.111.10-192.168.111.15
>>>>>>> upstream/master


# TEST (poner *[ok]) en la respuesta correcta

# Que ventajas tiene usar SNAT en vez de MASQUERADE:
* Ninguna ventaja, MASQUERADE se ha de usar siempre
* [ok]Si queremos tener mayor control sobre la ip pública o los puertos de salida que queremos usar para hacer nat hay que usar SNAT
* SNAT permite sustituir la dirección mac
* SNAT se puede usar en la cadena forward

# Si queremos que un servidor publico no acepte conexiones desde internet a su puerto ssh (22 tcp) que regla es la correcta:
* [ok]iptables -A PREROUTING -i eth0 -p tcp --dport 22 -j DROP
* iptables -A INPUT -o eth0 -p tcp --sport 22 -j DROP
* iptables -A INPUT -i eth0 -p tcp --sport 22 -j DROP
* iptables -A INPUT -i eth0 -p tcp --dport 22 -j REJECT

# Si queremos que un equipo sólo pueda navegar por internet en http / https necesitaremos filtrar:
* [ok]ACCEPT: puertos tcp 80 y 443, puerto UDP 53 y DROP todo lo demás
* ACCEPT: puertos tcp 80 y 443 y DROP todo lo demás
* ACCEPT: puertos tcp 80 y 443, puertos UDP 53 y 54 y DROP todo lo demás
* ACCEPT: puertos tcp 80 y puerto UDP 53 y DROP todo lo demás

# Si queremos dejar pasar las conexiones ya establecidas es mejor poner esta regla:

iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

* La primera de todas
* La última de todas
* No importa donde
* [ok]La regla no es correcta

# A partir de estas reglas:
/sbin/iptables -t nat -A PREROUTING -p tcp --dport 25 -i eth0 -j DNAT --to 192.168.0.100:25
/sbin/iptables -A FORWARD -p tcp -d 192.168.0.100 --dport 25 -j ACCEPT

* los correos electrónicos entrantes se redirigirán al servidor 192.168.0.100 si la interface de entrada es eth0
* los correos electrónicos entrantes se redirigirán al servidor 192.168.0.10 
* [ok]los correos electrónicos entrantes se redirigirán pero no pasarán porque se filtrarán en FORWARD
* los correos electrónicos entrantes se redirigirán al servidor desde cualquier interface 

