EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# adreça a la interface


# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  


# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# ports independents


# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# canviem nom perquè quedi més clar

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes


# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

##### RESETEJAR ROUTER

		[roger@j04 netswithlinux]$ telnet 192.168.88.1
		Trying 192.168.88.1...
		Connected to 192.168.88.1.
		Escape character is '^]'.

		MikroTik v6.33.5 (stable)
		Login: admin
		Password: 


		  MMM      MMM       KKK                          TTTTTTTTTTT      KKK
		  MMMM    MMMM       KKK                          TTTTTTTTTTT      KKK
		   MM MMMM MMM  III  KKK  KKK  RRRRRR     OOOOOO      TTT     III  KKK  KKK       MMM  MM  MMM  III  KKKKK     RRR  RRR  OOO  OOO     TTT     III  KKKKK



		  MikroTik RouterOS 6.33.5 (c) 1999-2015       http://www.mikrotik.com/

		[?]             Gives the list of available commands
		command [?]     Gives help on the command and list of arguments

		[Tab]           Completes the command/word. If the input is ambiguous,
						a second [Tab] gives possible options

		/               Move up to base level
		..              Move up one level
		/command        Use command at the base level
		jan/02/1970 00:00:16 system,error,critical router was rebooted without proper shu
		tdown
		jan/02/1970 00:00:17 system,error,critical router was rebooted without proper shu
		tdown
		  
		[admin@infMKT04] > /system reset-configuration 
		Dangerous! Reset anyway? [y/N]: 
		y
		system configuration will be reset
		Connection closed by foreign host.

		[roger@j04 netswithlinux]$ ip a a 192.168.88.4/24 dev enp0s18f1u3


##### AFEGIR CONFIGURACIÓ

		[admin@MikroTik] > # Cambiamos direccin IP para que la gestione directamente la interface 2
		[admin@MikroTik] > ip address set 0 interface=ether2-master
		[admin@MikroTik] > 
		[admin@MikroTik] > # Eliminamos el bridge
		[admin@MikroTik] > 
		[admin@MikroTik] > /interface bridge port remove 1
		[admin@MikroTik] > /interface bridge port remove 0
		[admin@MikroTik] > /interface bridge remove 0  
		[admin@MikroTik] > 
		[admin@MikroTik] > # Eliminamos que un puerto sea master de otro
		[admin@MikroTik] > 
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether3 ] master-port=none
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether4 ] master-port=none
		[admin@MikroTik] > 
		[admin@MikroTik] > # Cambiamos nombres a los puertos
		[admin@MikroTik] > 
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether1 ] name=eth1
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether2 ] name=eth2
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether3 ] name=eth3
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether4 ] name=eth4
		[admin@MikroTik] > 
		[admin@MikroTik] > # Deshabilitamos la wifi
		[admin@MikroTik] > 
		[admin@MikroTik] > /interface wireless set [ find default-name=wlan1 ] disabled=yes
		[admin@MikroTik] > 
		[admin@MikroTik] > # Eliminamos servidor y cliente dhcp
		[admin@MikroTik] > 
		[admin@MikroTik] > /ip pool remove 0
		[admin@MikroTik] > /ip dhcp-server network remove 0
		[admin@MikroTik] > /ip dhcp-server remove 0
		[admin@MikroTik] > /ip dhcp-client remove 0
		[admin@MikroTik] > /ip dns static remove 0
		[admin@MikroTik] > /ip dns set allow-remote-requests=no
		[admin@MikroTik] > 
		[admin@MikroTik] > # Eliminamos regla de nat masquerade
		[admin@MikroTik] > /ip firewall nat remove 0

		[admin@MikroTik] > /system identity set name=mkt04             
		[admin@mkt04] > /ip firewall nat remove 0  


Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt00
/system backup save name="20170317_zeroconf"

```


##### EXERCICIS


### [ejercicio1] Explica el procedimiento para resetear el router y restaurar
### este backup, verifica con /export que no queda ninguna configuración inicial 
### que pueda molestarnos para empezar a programar el router
	
	Per resetejar el router caldrà fer:
	
		[admin@mkt04] > /system reset-configuration 
		Dangerous! Reset anyway? [y/N]: 
		y
		system configuration will be reset
		Connection closed by foreign host.
	
	o mitjançant el botó físic reset del router.
	
	Per tal de no perdre tota la informació que conté el nostre router, caldrà fer un backup de la configuració abans de fer el reset.
	
		[admin@mkt04] > /system backup save name="20170317_zeroconf"
		[admin@mkt04] > /file print
		 # NAME                                                 TYPE                                                      SIZE CREATION-TIME       
		 0 skins                                                directory                                                      jan/01/1970 00:00:01
		 1 auto-before-reset.backup                             backup                                                 23.6KiB jan/02/1970 00:07:17
		 2 test.backup                                          backup                                                 19.8KiB jan/02/1970 01:33:58
		 3 20170317_zeroconf.backup                             backup                                                 23.6KiB jan/02/1970 00:06:21

	
	Un cop fet això resetegem el router com s'ha indicat anteriorment i procedim a la càrrega del backup (comprovant primer que no queda cap configuració inicial).
	
		[admin@mkt04] > /system reset-configuration 
		Dangerous! Reset anyway? [y/N]: 
		y
		system configuration will be reset
		Connection closed by foreign host.
		
		[admin@MikroTik] > /export
		# jan/02/1970 00:00:48 by RouterOS 6.33.5
		# software id = 3GAD-YVGP
		#
		/interface bridge
		add admin-mac=6C:3B:6B:C3:89:F9 auto-mac=no comment=defconf name=bridge
		/interface wireless
		set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce disabled=no distance=indoors frequency=auto mode=ap-bridge \
			ssid=MikroTik-C389FC wireless-protocol=802.11
		/interface ethernet
		set [ find default-name=ether2 ] name=ether2-master
		set [ find default-name=ether3 ] master-port=ether2-master
		set [ find default-name=ether4 ] master-port=ether2-master
		/ip neighbor discovery
		set ether1 discover=no
		set bridge comment=defconf
		/ip pool
		add name=default-dhcp ranges=192.168.88.10-192.168.88.254
		/ip dhcp-server
		add address-pool=default-dhcp disabled=no interface=bridge name=defconf
		/interface bridge port
		add bridge=bridge comment=defconf interface=ether2-master
		add bridge=bridge comment=defconf interface=wlan1
		/ip address
		add address=192.168.88.1/24 comment=defconf interface=bridge network=192.168.88.0
		/ip dhcp-client
		add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
		/ip dhcp-server network
		add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
		/ip dns
		set allow-remote-requests=yes
		/ip dns static
		add address=192.168.88.1 name=router
		/ip firewall filter
		add chain=input comment="defconf: accept ICMP" protocol=icmp
		add chain=input comment="defconf: accept establieshed,related" connection-state=established,related
		add action=drop chain=input comment="defconf: drop all from WAN" in-interface=ether1
		add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related
		add chain=forward comment="defconf: accept established,related" connection-state=established,related
		add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
		add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new \
			in-interface=ether1
		/ip firewall nat
		add action=masquerade chain=srcnat comment="defconf: masquerade" out-interface=ether1
		/system routerboard settings
		set cpu-frequency=650MHz protected-routerboot=disabled
		/tool mac-server
		set [ find default=yes ] disabled=yes
		add interface=bridge
		/tool mac-server mac-winbox
		set [ find default=yes ] disabled=yes
		add interface=bridge

	Com es pot veure no es manté la configuració que havíem carregat, per tant carreguem el backup de la següent manera.
		
		[admin@MikroTik] > system backup load name="20170317_zeroconf"
		password: 
		Restore and reboot? [y/N]: 
		y
		Restoring system configuration
		System configuration restored, rebooting now
		Connection closed by foreign host.
	
	Connectem de nou amb el router i comprovem que les configuracions són correctes.

		[admin@mkt04] > /export 
		# jan/02/1970 00:01:02 by RouterOS 6.33.5
		# software id = 3GAD-YVGP
		#
		/interface wireless
		set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce distance=indoors frequency=auto mode=ap-bridge ssid=\
			MikroTik-C389FC wireless-protocol=802.11
		/interface ethernet
		set [ find default-name=ether1 ] name=eth1
		set [ find default-name=ether2 ] name=eth2
		set [ find default-name=ether3 ] name=eth3
		set [ find default-name=ether4 ] name=eth4
		/ip neighbor discovery
		set eth1 discover=no
		/interface wireless security-profiles
		set [ find default=yes ] supplicant-identity=MikroTik
		/ip address
		add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
		/ip firewall filter
		add chain=input comment="defconf: accept ICMP" protocol=icmp
		add chain=input comment="defconf: accept establieshed,related" connection-state=established,related
		add action=drop chain=input comment="defconf: drop all from WAN" in-interface=eth1
		add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related
		add chain=forward comment="defconf: accept established,related" connection-state=established,related
		add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
		add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new \
			in-interface=eth1
		/system identity
		set name=mkt04
		/system routerboard settings
		set cpu-frequency=650MHz protected-routerboot=disabled
		/tool mac-server
		set [ find default=yes ] disabled=yes
		add
		/tool mac-server mac-winbox
		set [ find default=yes ] disabled=yes
		add


### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*1XX: red pública
*2XX: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 ssid="free104"
/interface wireless add ssid="private204" master-interface=wlan1
```
	Afegim les interfícies virtuals wifi addicionals i canviar els ssid:
	
		[admin@mkt04] > /interface wireless set [find name=wlan1] ssid="free104"
		[admin@mkt04] > /interface wireless add ssid="private204" master-interface=wlan1
		[admin@mkt04] > /interface wireless print               
		Flags: X - disabled, R - running 
		 0 X  name="wlan1" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:FC arp=enabled interface-type=Atheros AR9300 mode=ap-bridge 
			  ssid="free104" frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag 
			  vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes 
			  default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 

		 1 X  name="wlan2" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C3:89:FC arp=enabled interface-type=virtual-AP master-interface=wlan1 
			  ssid="private204" vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled 
			  default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no 
			  security-profile=default 

	

### [ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
### al hacer /interface print. Habilita y deshabilita estas interfaces. Cámbiales
### el nombre a wFree wPrivate y dejalas deshabilitadas

	Les interfícies wifi apareixen com a deshabilitades perquè per defecte estan deshabilitades (segons informació del manual).
	
	Per habilitar-les caldrà fer:
	
		[admin@mkt04] > /interface wireless set [find ssid="free104"] disabled=no       
		[admin@mkt04] > /interface wireless set [find ssid="private204"] disabled=no
		[admin@mkt04] > /interface wireless print 
		Flags: X - disabled, R - running 
		 0    name="wlan1" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:FC arp=enabled interface-type=Atheros AR9300 mode=ap-bridge 
			  ssid="free104" frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag 
			  vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes 
			  default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 

		 1    name="wlan2" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C3:89:FC arp=enabled interface-type=virtual-AP master-interface=wlan1 
			  ssid="private204" vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled 
			  default-authentication=yes default

	Per deshabilitar-les caldrà fer:
		
		[admin@mkt04] > /interface wireless set [find ssid="free104"] disabled=yes
		[admin@mkt04] > /interface wireless set [find ssid="private204"] disabled=yes
	
	Canviem el nom de les interfícies:
	
		[admin@mkt04] > /interface wireless set [find ssid="free104"] name="wFree"
		[admin@mkt04] > /interface wireless set [find ssid="private204"] name="wPrivate"
		
		[admin@mkt04] > interface wireless print                                        
		Flags: X - disabled, R - running 
		 0 X  name="wFree" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:FC arp=enabled interface-type=Atheros AR9300 mode=ap-bridge 
			  ssid="free104" frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag 
			  vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes 
			  default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 

		 1 X  name="wPrivate" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C3:89:FC arp=enabled interface-type=virtual-AP master-interface=wFree 
			  ssid="private204" vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled 
			  default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no 
			  security-profile=default 


		
## Crear interfaces virtuales y hacer bridges

```
/interface vlan add name eth4-vlan104 vlan-id=104 interface=eth4
/interface vlan add name eth4-vlan204 vlan-id=204 interface=eth4   

/interface bridge add name=br-vlan104
/interface bridge add name=br-vlan204


/interface bridge port add interface=eth4-vlan104 bridge=br-vlan104
/interface bridge port add interface=eth3 bridge=br-vlan104
/interface bridge port add interface=wFree  bridge=br-vlan104      


/interface bridge port add interface=eth4-vlan204 bridge=br-vlan204
/interface bridge port add interface=wPrivate   bridge=br-vlan204

/interface print

```
	Creem les interfícies virtuals i els bridges i comprovem que s'han creat correctament.
		[admin@mkt04] > /interface vlan add name eth4-vlan104 vlan-id=104 interface=eth4
		[admin@mkt04] > /interface vlan add name eth4-vlan204 vlan-id=204 interface=eth4   
		[admin@mkt04] > /interface bridge add name=br-vlan104
		[admin@mkt04] > /interface bridge add name=br-vlan204
		[admin@mkt04] > /interface bridge port add interface=eth4-vlan104 bridge=br-vlan104
		[admin@mkt04] > /interface bridge port add interface=eth3 bridge=br-vlan104
		[admin@mkt04] > /interface bridge port add interface=wFree  bridge=br-vlan104      
		[admin@mkt04] > /interface bridge port add interface=eth4-vlan204 bridge=br-vlan204
		[admin@mkt04] > /interface bridge port add interface=wPrivate   bridge=br-vlan204
		[admin@mkt04] > /interface print
		Flags: D - dynamic, X - disabled, R - running, S - slave 
		 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
		 0     eth1                                ether            1500  1598       2028 6C:3B:6B:C3:89:F8
		 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:C3:89:F9
		 2   S eth3                                ether            1500  1598       2028 6C:3B:6B:C3:89:FA
		 3     eth4                                ether            1500  1598       2028 6C:3B:6B:C3:89:FB
		 4  XS wFree                               wlan             1500  1600       2290 6C:3B:6B:C3:89:FC
		 5  X  wPrivate                            wlan                   1600       2290 6E:3B:6B:C3:89:FC
		 6  R  br-vlan104                          bridge           1500  1594            6C:3B:6B:C3:89:FB
		 7  R  br-vlan204                          bridge           1500  1594            6C:3B:6B:C3:89:FB
		 8   S eth4-vlan104                        vlan             1500  1594            6C:3B:6B:C3:89:FB
		 9   S eth4-vlan204                        vlan             1500  1594            6C:3B:6B:C3:89:FB



### [ejercicio3] Comenta cada una de estas líneas de la configuración
### anterior para que quede bien documentado. 

	`
	/interface vlan add name eth4-vlan104 vlan-id=104 interface=eth4  ## Afegim el nom eth4-vlan104 a la interfície eth4 i l'identificador de la vlan com 104.
	/interface vlan add name eth4-vlan204 vlan-id=204 interface=eth4  ## Afegim el nom eth4-vlan204 a la interfície eth4 i l'identificador de la vlan com 204. 
									
									## Així creem les interfícies virtuals.

	/interface bridge add name=br-vlan104 ## Afegim un bridge de nom br-vlan104.
	/interface bridge add name=br-vlan204 ## Afegim un bridge de nom br-vlan204.
	
									## Així creem els bridges.
	
	/interface bridge port add interface=eth4-vlan104 bridge=br-vlan104  ## Afegim el bridge br-vlan104 a la interfície eth4-vlan104.
	/interface bridge port add interface=eth3 bridge=br-vlan104			 ## Afegim el bridge br-vlan104 a la interfície eth3.
	/interface bridge port add interface=wFree  bridge=br-vlan104  		 ## Afegim el bridge br-vlan104 a la interfície wFree.    

								    ## Així afegim els bridges a les interfícies del wifi free.

	/interface bridge port add interface=eth4-vlan204 bridge=br-vlan204  ## Afegim el bridge br-vlan204 a la interfície eth4-vlan204.
	/interface bridge port add interface=wPrivate   bridge=br-vlan204    ## Afegim el bridge br-vlan104 a la interfície wPrivate. 
	
									## Així afegim els bridges a les interfícies del wifi private.
									
	/interface print
	
	`
	

### [ejercicio4] Pon una ip 172.17.104.1/24 a br-vlan104 
### y 172.17.204.1/24 a br-vlan204 y verifica desde la interface de la placa 
### base de tu ordenador que hay conectividad (puedes hacer ping) configurando
### unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración.


	[admin@mkt04] > ip address add address=172.17.104.1/24 interface=br-vlan104 
	[admin@mkt04] > ip address add address=172.17.204.1/24 interface=br-vlan204  
	
	Amb /export comprovem que les ip's han estat creades:
	
		/ip address
		add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
		add address=172.17.104.1/24 interface=br-vlan104 network=172.17.104.0
		add address=172.17.204.1/24 interface=br-vlan204 network=172.17.204.0


	[admin@mkt04] > ping 172.17.104.100  
	  SEQ HOST                                     SIZE TTL TIME  STATUS                                                                       
		0 172.17.104.100                             56  64 0ms  
		1 172.17.104.100                             56  64 0ms  
		2 172.17.104.100                             56  64 0ms  
		3 172.17.104.100                             56  64 0ms  
		4 172.17.104.100                             56  64 0ms  
		5 172.17.104.100                             56  64 0ms  
		sent=6 received=6 packet-loss=0% min-rtt=0ms avg-rtt=0ms max-rtt=0ms 


	Fem el backup de la configuració:

	[admin@mkt04] > /system backup save name="20170321_confIPs"      
	Saving system configuration
	Configuration backup saved
	
	[admin@mkt04] > file print
	 # NAME                                                 TYPE                                                      SIZE CREATION-TIME       
	 0 skins                                                directory                                                      jan/01/1970 00:00:01
	 1 auto-before-reset.backup                             backup                                                 23.6KiB jan/02/1970 00:26:51
	 2 20170321_confIPs.backup                              backup                                                 24.4KiB jan/02/1970 00:46:46
	 3 20170317_confexercici2.backup                        backup                                                 24.1KiB jan/02/1970 00:06:54
	 4 test.backup                                          backup                                                 19.8KiB jan/02/1970 01:33:58
	 5 20170317_zeroconf.backup                             backup                                                 23.6KiB jan/02/1970 00:06:21


	

### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
### .101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
### Crear reglas de nat para salir a internet. 

	Creem els rangs (101-250) del servidor dhcp per cadascuna de les xarxes:

		[admin@mkt04] > /ip pool add ranges=172.17.104.101-172.17.104.250 name=range_public 
		[admin@mkt04] > /ip pool add ranges=172.17.204.101-172.17.204.250 name=range_private


	Afegim una ip a cada interfície bridge: 
	
		[admin@mkt04] > ip address add interface=br-vlan104 address=172.17.104.104 netmask=255.255.255.0
		[admin@mkt04] > ip address add interface=br-vlan204 address=172.17.204.204 netmask=255.255.255.0 


	Afegim a cada bridge el rang corresponent d'ips (public --> rang public, privat --> rang private):
	
		[admin@mkt04] > /ip dhcp-server add interface=br-vlan104 address-pool=range_public   
		[admin@mkt04] > /ip dhcp-server add interface=br-vlan204 address-pool=range_private   


	Afegim els defaults gateways per cada bridge:
	
		[admin@mkt04] > /ip dhcp-server network add gateway=172.17.104.1 dns-server=8.8.8.8 netmask=255.255.255.0 address=172.17.104.0/24
		[admin@mkt04] > /ip dhcp-server network add gateway=172.17.204.1 dns-server=8.8.8.8 netmask=255.255.255.0 address=172.17.204.0/24 


	Afegim ip a la eth1:
	
		[admin@mkt04] > ip address add interface=eth1 address=192.168.3.204/16
	
	
	Comprovem que hem creat les pools:
		
		[admin@mkt04] > ip pool print
		 # NAME                                         RANGES                         
		 0 range_public                                 172.17.104.101-172.17.104.250  
		 1 range_private                                172.17.204.101-172.17.204.250  
		 
		 
	Habilitem el servidor dhcp:
	
		[admin@mkt04] > ip dhcp-server enable dhcp1 # Public
		[admin@mkt04] > ip dhcp-server enable dhcp2 # Privat
		
		[admin@mkt04] > /ip dhcp-server print        
		Flags: X - disabled, I - invalid 
		 #   NAME                         INTERFACE                         RELAY           ADDRESS-POOL                         LEASE-TIME ADD-ARP
		 0   dhcp1                        br-vlan104                                        range_public                         10m       
		 1   dhcp2                        br-vlan204                                        range_private                        10m     

						
	Posem les ip's a l'ordinador:
	
		[root@j04 ~]# ip link add link enp2s0 name v104 type vlan id 104
		[root@j04 ~]# ip link add link enp2s0 name v204 type vlan id 204
		[root@j04 ~]# ip a
		
			7: v104@enp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
				link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
			8: v204@enp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
				link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
			
		[root@j04 ~]# ip a a 172.17.104.4/24 dev v104
		[root@j04 ~]# ip a a 172.17.204.4/24 dev v204
		[root@j04 ~]# ip a
		
			7: v104@enp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
				link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
				inet 172.17.104.4/24 scope global v104
				   valid_lft forever preferred_lft forever
			8: v204@enp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
				link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
				inet 172.17.204.4/24 scope global v204
				   valid_lft forever preferred_lft forever
			   
		[root@j04 ~]# ip link set v104 up
		[root@j04 ~]# ip link set v204 up
		[root@j04 ~]# ip a
		
			7: v104@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
				link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
				inet 172.17.104.4/24 scope global v104
				   valid_lft forever preferred_lft forever
			8: v204@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> > mtu 1500 qdisc noqueue state UP group default qlen 1000
				link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
				inet 172.17.204.4/24 scope global v204
				   valid_lft forever preferred_lft forever

			
	Regles de nat:
		
		[admin@mkt04] > /ip firewall nat add action=masquerade chain=srcnat out-interface=eth1
		
		Configurem les rutes perquè eth1 sigui la sortida a internet
		 
			[admin@mkt04] > /ip route add distance=1 gateway=192.168.0.1
			[admin@mkt04] > /ip address export
			# jan/02/1970 00:06:07 by RouterOS 6.33.5
			# software id = 3GAD-YVGP
			#
			/ip address
			add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
			add address=172.17.104.1/24 interface=br-vlan104 network=172.17.104.0
			add address=172.17.204.1/24 interface=br-vlan204 network=172.17.204.0
			add address=172.17.104.104/24 interface=br-vlan104 network=172.17.104.0
			add address=172.17.204.204/24 interface=br-vlan204 network=172.17.204.0
			add address=192.168.3.204/24 interface=eth1 network=192.168.3.0
			add address=192.168.3.204/16 interface=eth1 network=192.168.0.0

		Fem un ping per comprovar que realment funciona
		
			[admin@mkt04] > ping 8.8.8.8
			  SEQ HOST                                     SIZE TTL TIME  STATUS                                                                       
				0 8.8.8.8                                    56  56 10ms 
				1 8.8.8.8                                    56  56 10ms 
				2 8.8.8.8                                    56  56 11ms 
				3 8.8.8.8                                    56  56 11ms 
				4 8.8.8.8                                    56  56 10ms 
				5 8.8.8.8                                    56  56 10ms 
				6 8.8.8.8                                    56  56 11ms 
				sent=7 received=7 packet-loss=0% min-rtt=10ms avg-rtt=10ms max-rtt=11ms 


### [ejercicio6] Activar redes wifi y dar seguridad wpa2

	Activem les wireless, tant la privada com la pública:
	
		[admin@mkt04] > interface wireless set [find ssid=mktf04] disabled=no
		[admin@mkt04] > interface wireless set [find ssid=mktp04] disabled=no 

	
	Creem la contrassenya amb seguretat wpa2 i afegim la configuració del perfil de seguretat a la xarxa wFree:
	
		[admin@mkt04] > interface wireless security-profiles add authentication-types=wpa2-psk wpa2-pre-shared-key="freemkt04" name=wFree mode=dynamic-keys supplicant-identity=mkt04
		[admin@mkt04] > interface wireless set numbers=wFree security-profile=wFree

	Creem la contrassenya amb seguretat wpa2 i afegim la configuració del perfil de seguretat a la xarxa wPrivate:
	
		[admin@mkt04] > interface wireless security-profiles add authentication-types=wpa2-psk wpa2-pre-shared-key="privatemkt04" name=wPrivate mode=dynamic-keys supplicant-identity=mkt04
		[admin@mkt04] > interface wireless set numbers=wPrivate security-profile=wPrivate


	Comprovem que s'han creat correctament els perfils i que les xarxes wifi tenen el security-profile corresponent:

		[admin@mkt04] > interface wireless security-profiles print
		Flags: * - default 
		
		 1   name="wFree" mode=dynamic-keys authentication-types=wpa2-psk unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa-pre-shared-key="" 
			 wpa2-pre-shared-key="freemkt04" supplicant-identity="mkt04" eap-methods="" tls-mode=no-certificates tls-certificate=none 
			 mschapv2-username="" mschapv2-password="" static-algo-0=none static-key-0="" static-algo-1=none static-key-1="" static-algo-2=none 
			 static-key-2="" static-algo-3=none static-key-3="" static-transmit-key=key-0 static-sta-private-algo=none static-sta-private-key="" 
			 radius-mac-authentication=no radius-mac-accounting=no radius-eap-accounting=no interim-update=0s radius-mac-format=XX:XX:XX:XX:XX:XX 
			 radius-mac-mode=as-username radius-mac-caching=disabled group-key-update=5m management-protection=disabled 
			 management-protection-key="" 

		 2   name="wPrivate" mode=dynamic-keys authentication-types=wpa2-psk unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa-pre-shared-key="" 
			 wpa2-pre-shared-key="privatemkt04" supplicant-identity="mkt04" eap-methods="" tls-mode=no-certificates tls-certificate=none 
			 mschapv2-username="" mschapv2-password="" static-algo-0=none static-key-0="" static-algo-1=none static-key-1="" static-algo-2=none 
			 static-key-2="" static-algo-3=none static-key-3="" static-transmit-key=key-0 static-sta-private-algo=none static-sta-private-key="" 
			 radius-mac-authentication=no radius-mac-accounting=no radius-eap-accounting=no interim-update=0s radius-mac-format=XX:XX:XX:XX:XX:XX 
			 radius-mac-mode=as-username radius-mac-caching=disabled group-key-update=5m management-protection=disabled 
			 management-protection-key="" 

		[admin@mkt04] > interface wireless print                  
		Flags: X - disabled, R - running 
		 0    name="wFree" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:FC arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="mktf04" 
			  frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 
			  wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes 
			  default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=wFree compression=no 

		 1    name="wPrivate" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C3:89:FC arp=enabled interface-type=virtual-AP master-interface=wFree 
			  ssid="mktp04" vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled 
			  default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no 
			  security-profile=wPrivate 



### [ejercicio6b] Opcional (montar portal cautivo)

	Afegim un hotspot a la wifi pública:

		[admin@mkt04] > ip hotspot setup 
		Select interface to run HotSpot on 

		hotspot interface: wFree
		Set HotSpot address for interface 

		local address of network: 192.168.3.104
		masquerade network: yes
		Set pool for HotSpot addresses 

		address pool of network: 172.17.104.101-172.17.104.250
		Select hotspot SSL certificate 

		select certificate: none
		Select SMTP server 

		ip address of smtp server: 0.0.0.0
		Setup DNS configuration 

		dns servers: 
		DNS name of local hotspot server 

		dns name: hotspotpublic
		Create local hotspot user 

		name of local hotspot user: admin
		password for the user: 
		


### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar puertos en Free.

	PORT TELNET --> 23

	PORT SSH --> 22

	HTTP --> 80

	DNS --> UDP 53

	HTTPS --> 443

	CANVIEM DE PORTS PER A QUE NINGÚ SÀPIGA ON ES TROBEN OBERTS, per exemple posar el ssh a 22340

	WINBOX --> IP --> services

	Canviem el port del ssh:
		[admin@mkt04] > ip service export 
		# apr/04/2017 13:40:39 by RouterOS 6.33.5
		# software id = 3GAD-YVGP
		#
		/ip service
		set ssh port=22340

	Creem una regla perquè es redireccioni al ssh quan s'entra per el port 22340
	
		[admin@mkt04] > ip firewall filter export 
		# apr/04/2017 13:44:24 by RouterOS 6.33.5
		# software id = 3GAD-YVGP
		#
		/ip firewall filter
		add chain=input comment="defconf: accept ICMP" protocol=icmp
		add chain=input comment="defconf: accept ssh on port 22340" dst-port=22340 \
			protocol=tcp
		add chain=input comment="defconf: accept establieshed,related" \
			connection-state=established,related
		add action=drop chain=input comment="defconf: drop all from WAN" \
			in-interface=eth1
		add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
			connection-state=established,related
		add chain=forward comment="defconf: accept established,related" \
			connection-state=established,related
		add action=drop chain=forward comment="defconf: drop invalid" \
			connection-state=invalid
		add action=drop chain=forward comment=\
			"defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat \
			connection-state=new in-interface=eth1

	Posem passwd al router:
	
		[admin@mkt04] > password              
		old-password: 
		new-password: *****
		confirm-new-password: *****

		SYSTEM SNTP CLIENT --> ENABLE


	Bloquejar comunicacions entre wFree i wPrivate:
	
		[admin@mkt04] > ip firewall add action=drop chain=input comment=\
		"Firewall drop packets from wFree (wPriv)" dst-address=172.17.204.0/24 \
		src-address=172.17.104.0/24
		
		[admin@mkt04] > ip firewall add action=drop chain=input comment=\
		"Firewall drop packets from wPrivate (wFree)" dst-address=172.17.104.0/24 \
		src-address=172.17.204.0/24



### [ejercicio8] Conexión a switch cisco con vlans





### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante




### [ejercicio9] QoS





### [ejercicio9] Balanceo de salida a internet





### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos





### [ejercicio11] Firewall avanzado








