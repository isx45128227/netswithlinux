# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 |172.16.4.0/25 | 172.16.4.127 | 126 dispositius - 172.16.4.1 --> 172.16.4.126
174.187.55.6/23 |174.187.54.0/23 |  174.187.55.255 | 510 dispositius - 174.187.54.1 --> 174.187.55.254
10.0.25.253/18 |10.0.0.0/18 | 10.0.63.255 | 16382 dispositius - 10.0.0.1 --> 10.0.63.254
209.165.201.30/27 |209.165.201.0/27 | 209.165.201.31   | 30 dispositius - 209.165.201.1 --> 209.165.201.30


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.
    La màscara de xarxa que necessitarem per fer 80 subxarxes serà: 255.255.254.0 = 23, 
    ja que hem d'agafar 7 bits que són: 2⁷=128 combinacions possibles.
    amb una CIDR /23 podrem fer 128 subxarxes en la que cada xarxa pugui tenir fins a 32-23= 9 bits per host -->  2⁹-2 = 510 dispositius


##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.
        
    Per poder adreçar 1500 dispositius necessitarem una màscara CIDR /21, donat que 2¹¹ - 2 = 2046 dispositius. 
    La màscara serà doncs: 255.255.248.0 que en binari és: 11111111.11111111.11111000.00000000


b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?
    
    2²² --> 4194302 dispositius 
    Si fixem l'adreça a 10.192.0.0/10, podrem fer 4194302/1500 = 2796 subxarxes.
    


c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.
    
    Si la xarxa és 10.192.0.0/21 i necessitem 3 subxarxes de 500 dispositius, la nova adreça de màscara serà: 255.255.254.0, 
    ja que agafem 2 bits per fer 3 subxarxes i ens permet tenir 32-23 = 9, 2⁹-2 = 510 dispositius cadascuna.
    En decimal serà: 255.255.254.0
    En binari serà: 11111111.11111111.11111110.00000000
    
    
##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius
a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.
    
    Per poder adreçar 3250 dispositius necessitarem una màscara CIDR /20, donat que 2¹² - 2 = 4094 dispositius.
    La màscara serà doncs: 255.255.240.0 que en binari és: 11111111.11111111.11110000.00000000

b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.
    
    Si la xarxa és 10.128.0.0/20 i necessitem 5 subxarxes de 650 dispositius, la nova adreça de màscara serà: 255.255.254.0, 
    ja que agafem 3 bits per fer 5 subxarxes (2³ = 8 subxarxes possibles) i ens permet tenir 32 - 23 = 9, 2⁹-2 = 510 dispositius cadascuna.
    En decimal serà: 255.255.254.0
    En binari serà: 11111111.11111111.11111110.00000000
    No es pot realitzar una nova màscara que permeti adreçar 650 dispositius, 
    donat que el màxim a adreçar per a 5 subxarxes són 510 dispositius cadascuna.

c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.
    
    Cada subxarxa NO podrà adreçar 650 dispositius donat que si agafem 3 bits per a les 8 combinacions possibles de subxarxes 
    (que en el nostre cas només necessitem 5), necessitarem per força una màscara CIDR /23 i si calculem 32 - 23 = 9, 2⁹-2 = 510 dispositius, 
    podem observar que cada subxarxa tindrà com a màxim 510 dispositius.
    
     Fórmula: Bits totals (32) - Bits de la màscara de xarxa = bits de host, 
     i després fem: 2^nombre de bits de host que hem obtingut - 2 i obtindrem els dispositius totals.

d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.
    
    Si, podem fer dues subxarxes amb 1625 dispositius cadascuna. Per això:
    Per construir 2 subxarxes només necessitem 2 bits, per tant la nova màscara CIDR haurà de ser /21 i observem que:
    32 - 21 = 11, 2¹¹ - 2 = 2046 dispositius. 
    Per tant, si és possible fer dues subxarxes de la xarxa mare de l'apartat (a) amb 1625 dispositius cadascuna.
    




#### 5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.
a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?
    
    Necessitarem 3 bits, donat que 2³ = 8, per tant podrem fer fins a 8 subxarxes diferents. 
    Així doncs passarem d'una màscara CIDR /12 a una màscara CIDR /15.
    
b) Dóna la nova MX, tant en format decimal com en binari.
    
    La nova màscara de xarxa serà: 255.254.0.0, que equival a dir CIDR /15.
    En binari serà: 11111111.11111110.00000000.00000000
    En decimal serà: 255.254.0.0


c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1| 172.16.0.0/15 -- 10101100.00010000.00000000.00000000 | 172.17.255.255 -- 10101100.00010001.11111111.11111111 | 1 al 254
Xarxa 2| 172.18.0.0/15 -- 10101100.00010010.00000000.00000000 | 172.19.255.255 -- 10101100.00010011.11111111.11111111 | 1 al 254
Xarxa 3| 172.20.0.0/15 -- 10101100.00010100.00000000.00000000 | 172.21.255.255 -- 10101100.00010101.11111111.11111111 | 1 al 254
Xarxa 4| 172.22.0.0/15 -- 10101100.00010110.00000000.00000000 | 172.23.255.255 -- 10101100.00010111.11111111.11111111 | 1 al 254
Xarxa 5| 172.24.0.0/15 -- 10101100.00011000.00000000.00000000 | 172.25.255.255 -- 10101100.00011001.11111111.11111111 | 1 al 254
Xarxa 6| 172.26.0.0/15 -- 10101100.00011010.00000000.00000000 | 172.27.255.255 -- 10101100.00011011.11111111.11111111 | 1 al 254


d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

    Amb 3 bits podem fer fins a 8 subxarxes diferents. Ho podem calcular de la següent manera:
    Sabem que 2² = 4, però nosaltres necessitem 8 subxarxes, per tant necessitem 2³ = 8 bits. 
    Per a cada subxarxa necessitem 1 bit diferent, així que amb 2³ bits, tenim 8 combinacions possibles. 
    
    Fórmula: 2^n bits on n va de 0 al 7.

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.
    
    Segons la nova màscara de xarxa que hem calculat podem calcular els dispositius que podrem tenir a cada subxarxa fent: 32 - 15 = 17,
    i calculant 2¹⁷ - 2 = 131070 dispositius.
    
    Fórmula: Bits totals (32) - Bits de la màscara de xarxa = bits de host, 
    i després fem: 2^nombre de bits de host que hem obtingut - 2 i obtindrem els dispositius totals. 
    


##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 4 subxarxes?
    
    Necessitarem 2 bits, donat que 2² = 4, per tant podrem fer fins a 4 subxarxes diferents. 
    Així doncs passarem d'una màscara CIDR /24 a una màscara CIDR /26.
    

b) Dóna la nova MX, tant en format decimal com en binari.

    La nova màscara de xarxa serà: 255.255.255.192, que equival a dir CIDR /26.
    En binari serà: 11111111.11111111.11111111.11000000
    En decimal serà: 255.255.255.192

c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1| 192.168.1.0/26 -- 11000000.10101000.00000001.00000000 | 192.168.1.63 -- 11000000.10101000.00000001.00111111 | 1 al 62
Xarxa 2| 192.168.1.64/26 -- 11000000.10101000.00000001.01000000 | 192.168.1.127 -- 11000000.10101000.00000001.01111111 | 65 al 126
Xarxa 3| 192.168.1.128/26 -- 11000000.10101000.00000001.10000000 | 192.168.1.191 -- 11000000.10101000.00000001.10111111 | 129 al 190
Xarxa 4| 192.168.1.192/26 -- 11000000.10101000.00000001.11000000 | 192.168.1.255 --11000000.10101000.00000001.11111111 | 193 al 254


d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.
    
    Amb 2 bits podem fer fins a 4 subxarxes diferents. Ho podem calcular de la següent manera:
    Sabem que 2² = 4, per tant podem fer 4 subxarxes diferents. 
    Per a cada subxarxa necessitem 1 bit diferent, així que amb 2² bits, tenim 4 combinacions possibles. 
    Per tant, no podríem fer més de 4 subxarxes amb el nombre de bits calculat a l'apartat (a).
    
    Fórmula: 2^n bits on n va de 0 al 7.


e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.
    
    Segons la nova màscara de xarxa que hem calculat podem calcular els dispositius que podrem tenir a cada subxarxa fent: 32 - 26 = 6,
    i calculant 2⁶ - 2 = 62 dispositius.
    
    Fórmula: Bits totals (32) - Bits de la màscara de xarxa = bits de host, 
    i després fem: 2^nombre de bits de host que hem obtingut - 2 i obtindrem els dispositius totals.
    
