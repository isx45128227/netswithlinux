#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"

	# ip r f all
	
	[root@j04 netswithlinux]# ip r f all
	
	
	# ip a f dev enp2s0
	
	[root@j04 netswithlinux]# ip a f dev enp2s0.
	
	
	# ip a
	
	[root@j04 netswithlinux]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
	
	
	# ip r
	
	[root@j04 netswithlinux]# ip r



Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25
 
	# ip a a 2.2.2.2/24 dev enp2s0
	# ip a a 3.3.3.3/16 dev enp2s0
	# ip a a 4.4.4.4/25 dev enp2s0
	# ip a s dev enp2s0
	
	[root@j04 netswithlinux]# ip a a 2.2.2.2/24 dev enp2s0
	[root@j04 netswithlinux]# ip a a 3.3.3.3/16 dev enp2s0
	[root@j04 netswithlinux]# ip a a 4.4.4.4/25 dev enp2s0
	[root@j04 netswithlinux]# ip a s dev enp2s0
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
		inet 3.3.3.3/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 4.4.4.4/25 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 2.2.2.2/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	
	
	# ip a
	
	[root@j04 netswithlinux]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
		inet 3.3.3.3/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 4.4.4.4/25 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 2.2.2.2/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	3: enp0s18f1u3: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff



Consulta la tabla de rutas de tu equipo

	# ip r
	
	[root@j04 netswithlinux]# ip r
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 




Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

	# ping 2.2.2.2
	
	[root@j04 netswithlinux]# ping 2.2.2.2
	PING 2.2.2.2 (2.2.2.2) 56(84) bytes of data.
	64 bytes from 2.2.2.2: icmp_seq=1 ttl=64 time=0.035 ms
	64 bytes from 2.2.2.2: icmp_seq=2 ttl=64 time=0.043 ms
	64 bytes from 2.2.2.2: icmp_seq=3 ttl=64 time=0.064 ms
	64 bytes from 2.2.2.2: icmp_seq=4 ttl=64 time=0.062 ms
	^C
	--- 2.2.2.2 ping statistics ---
	4 packets transmitted, 4 received, 0% packet loss, time 3000ms
	rtt min/avg/max/mdev = 0.035/0.051/0.064/0.012 ms


	# ping 2.2.2.254
	
	[root@j04 netswithlinux]# ping 2.2.2.254
	PING 2.2.2.254 (2.2.2.254) 56(84) bytes of data.
	From 2.2.2.2 icmp_seq=1 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=2 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=3 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=4 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=5 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=6 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=7 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=8 Destination Host Unreachable
	^C
	--- 2.2.2.254 ping statistics ---
	8 packets transmitted, 0 received, +8 errors, 100% packet loss, time 7000ms

	
	# ping 2.2.5.2
	
	[root@j04 netswithlinux]# ping 2.2.5.2
	connect: Network is unreachable
	
	
	# ping 3.3.3.35
	
	[root@j04 netswithlinux]# ping 3.3.3.35
	PING 3.3.3.35 (3.3.3.35) 56(84) bytes of data.
	From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=2 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=3 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=4 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=5 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=6 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=7 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=8 Destination Host Unreachable
	^C
	--- 3.3.3.35 ping statistics ---
	8 packets transmitted, 0 received, +8 errors, 100% packet loss, time 7000ms


	# ping 3.3.200.45
	
	[root@j04 netswithlinux]# ping 3.3.200.45
	PING 3.3.200.45 (3.3.200.45) 56(84) bytes of data.
	From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=2 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=3 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=4 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=5 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=6 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=7 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=8 Destination Host Unreachable
	^C
	--- 3.3.200.45 ping statistics ---
	8 packets transmitted, 0 received, +8 errors, 100% packet loss, time 7000ms


	# ping 4.4.4.8
	
	[root@j04 netswithlinux]# ping 4.4.4.8
	PING 4.4.4.8 (4.4.4.8) 56(84) bytes of data.
	From 4.4.4.4 icmp_seq=1 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=2 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=3 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=4 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=5 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=6 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=7 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=8 Destination Host Unreachable
	^C
	--- 4.4.4.8 ping statistics ---
	9 packets transmitted, 0 received, +8 errors, 100% packet loss, time 8002ms


	# ping 4.4.4.132
	
	[root@j04 netswithlinux]# ping 4.4.4.132
	connect: Network is unreachable


	Network is unreachable vol dir que la xarxa està fora del nostre domini, i per tant no es pot fer un ping. 
	Destination Host Unreachable vol dir que el host en concret no està operatiu.

	
#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	# ip r f all
	
	[root@j04 netswithlinux]# ip r f all


	# ip a f dev enp2s0
	
	[root@j04 netswithlinux]# ip a f dev enp2s0
	
	
	# ip a
	
	[root@j04 netswithlinux]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff




Conecta una segunda interfaz de red por el puerto usb

	# dmesg
	
	[root@j04 netswithlinux]# dmesg
	[ 8776.664323] usb 4-3: new full-speed USB device number 2 using ohci-pci
	[ 8776.811357] usb 4-3: New USB device found, idVendor=0fe6, idProduct=9700
	[ 8776.811369] usb 4-3: New USB device strings: Mfr=0, Product=2, SerialNumber=0
	[ 8776.811376] usb 4-3: Product: USB 2.0 10/100M Ethernet Adaptor
	[ 8777.028581] dm9601 4-3:1.0 eth0: register 'dm9601' at usb-0000:00:12.1-3, Davicom DM96xx USB 10/100 Ethernet, 00:e0:4c:53:44:58
	[ 8777.028954] usbcore: registered new interface driver dm9601
	[ 8777.040892] usbcore: registered new interface driver sr9700
	[ 8777.068041] dm9601 4-3:1.0 enp0s18f1u3: renamed from eth0
	[ 8777.079293] IPv6: ADDRCONF(NETDEV_UP): enp0s18f1u3: link is not ready
	[ 8777.158298] dm9601 4-3:1.0 enp0s18f1u3: link up, 100Mbps, full-duplex, lpa 0xFFFF
	
	
	# ip a
	
	[root@j04 netswithlinux]# ip a
	[...]
	3: enp0s18f1u3: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff

Cambiale el nombre a usb0

	# ip link set enp0s18f1u3 down

	[root@j04 netswithlinux]# ip link set enp0s18f1u3 down
	[root@j04 netswithlinux]# ip a
	[...]
	3: enp0s18f1u3: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff


	# ip link set enp0s18f1u3 name usb0
	
	[root@j04 netswithlinux]# ip link set enp0s18f1u3 name usb0
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff


	# ip link set usb0 up
	
	[root@j04 netswithlinux]# ip link set usb0 up
	[root@j04 netswithlinux]# ip a
	[...]	
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff



Modifica la dirección MAC

	# ip link set usb0 down
	
	[root@j04 netswithlinux]# ip link set usb0 down
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	
	
	# ip link set usb0 address 00:11:22:33:44:55
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff

		
	# ip link set usb0 up

	[root@j04 netswithlinux]# ip link set usb0 up
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff


Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

	# ip a a 5.5.5.5/24 dev usb0
	[root@j04 netswithlinux]# ip a a 5.5.5.5/24 dev usb0
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff
		inet 5.5.5.5/24 scope global usb0
		   valid_lft forever preferred_lft forever

	
	# ip a a 7.7.7.7/24 dev enp2s0
	[root@j04 netswithlinux]# ip a a 7.7.7.7/24 dev enp2s0
	[root@j04 netswithlinux]# ip a
	[...]
	
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
		inet 3.3.3.3/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 4.4.4.4/25 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 2.2.2.2/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 192.168.3.4/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 20260sec preferred_lft 20260sec
		inet 7.7.7.7/24 scope global enp2s0
		   valid_lft forever preferred_lft forever

	

Observa la tabla de rutas

	[root@j04 netswithlinux]# ip r
	default via 192.168.0.5 dev enp2s0 
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 
	5.5.5.0/24 dev usb0  proto kernel  scope link  src 5.5.5.5 linkdown 
	7.7.7.0/24 dev enp2s0  proto kernel  scope link  src 7.7.7.7 
	192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.4




#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	# ip r f all
	
	[root@j04 netswithlinux]# ip r f all


	# ip a f dev enp2s0
	
	[root@j04 netswithlinux]# ip a f dev enp2s0


	# ip a 
	[root@j04 netswithlinux]# ip a
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

	# ip a a 172.16.99.XX/24
	
	[root@j04 netswithlinux]# ip a a 172.16.99.04 dev enp2s0
	
	
	# ip a 
	
	[root@j04 netswithlinux]# ip a
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
    inet 172.16.99.4/32 scope global enp2s0
       valid_lft forever preferred_lft forever

	
Lanzar iperf en modo servidor en cada ordenador

	# Servidor

	[root@j04 netswithlinux]# iperf -s
	------------------------------------------------------------
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)
	------------------------------------------------------------



Comprueba con netstat en qué puerto escucha

	# netstat

	[root@j04 netswithlinux]# netstat
	Active Internet connections (w/o servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State      
	tcp        0      0 j04.informatica.e:44666 104.210.2.228:https     ESTABLISHED
	tcp        0      0 j04.informatica.esc:868 gandhi.informatica.:nfs ESTABLISHED
	tcp        0      0 j04.informatica.e:44668 104.210.2.228:https     ESTABLISHED
	tcp        0      0 j04.informatica.e:40294 192.0.73.2:https        ESTABLISHED
	tcp        0      0 j04.informatica.e:44664 104.210.2.228:https     ESTABLISHED
	tcp        0      0 j04.informatica.e:44318 104.210.2.228:ssh       TIME_WAIT  
	udp6       0      0 localhost:58980         localhost:58980         ESTABLISHED
	Active UNIX domain sockets (w/o servers)
	Proto RefCnt Flags       Type       State         I-Node   Path
	unix  2      [ ]         DGRAM                    16136    /var/run/chrony/chronyd.sock
	unix  2      [ ]         DGRAM                    22055    /run/user/42/systemd/notify
	unix  2      [ ]         DGRAM                    25138    /run/user/202163/systemd/notify
	[...]


Conectarse desde otro pc como cliente


	# Client
	
	[root@j04 netswithlinux]# iperf -c 192.168.3.4
	
	
	# Servidor veu que s'ha connectat
	
	[root@j04 netswithlinux]# iperf -s
	
	------------------------------------------------------------
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)
	------------------------------------------------------------
	[  4] local 192.168.3.4 port 5001 connected with 192.168.3.5 port 57658
	[ ID] Interval       Transfer     Bandwidth
	[  4]  0.0-10.0 sec   112 MBytes  94.1 Mbits/sec

	


Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

	# tshark -c 30
	
	[root@j04 ~]# tshark -c 30
	Running as user "root" and group "root". This could be dangerous.
	Capturing on 'enp2s0'
	  1 0.000000000 192.168.0.10 → 192.168.3.4  TCP 66 389→58156 [FIN, ACK] Seq=1 Ack=1 Win=279 Len=0 TSval=3215244 TSecr=1675576
	  2 0.000462063  192.168.3.4 → 192.168.0.10 TCP 66 58156→389 [FIN, ACK] Seq=1 Ack=2 Win=273 Len=0 TSval=1737894 TSecr=3215244
	  3 0.000632945 192.168.0.10 → 192.168.3.4  TCP 66 389→58156 [ACK] Seq=2 Ack=2 Win=279 Len=0 TSval=3215245 TSecr=1737894
	  4 0.326624756 fe80::76d0:2bff:fec9:f796 → ff02::2      ICMPv6 62 Router Solicitation
	  5 0.547705199 192.168.3.16 → 224.0.0.251  MDNS 177 Standard query response 0x0000 PTR, cache flush j16.local AAAA, cache flush fe80::76d0:2bff:fec9:f796
	  6 0.547778116 fe80::76d0:2bff:fec9:f796 → ff02::fb     MDNS 197 Standard query response 0x0000 PTR, cache flush j16.local AAAA, cache flush fe80::76d0:2bff:fec9:f796
	  7 1.684583516      0.0.0.0 → 224.0.0.22   IGMPv3 60 Membership Report / Leave group 224.0.0.251
	  8 1.877854606  192.168.3.5 → 192.168.3.4  TCP 74 57662→5001 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM=1 TSval=242957 TSecr=0 WS=128
	  9 1.877961917  192.168.3.4 → 192.168.3.5  TCP 74 5001→57662 [SYN, ACK] Seq=0 Ack=1 Win=28960 Len=0 MSS=1460 SACK_PERM=1 TSval=1739772 TSecr=242957 WS=128
	 10 1.878161602  192.168.3.5 → 192.168.3.4  TCP 66 57662→5001 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=242957 TSecr=1739772
	 11 1.878234321  192.168.3.5 → 192.168.3.4  TCP 102 57662→5001 [PSH, ACK] Seq=1 Ack=1 Win=29312 Len=36 TSval=242957 TSecr=1739772
	 12 1.878256286  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=37 Win=29056 Len=0 TSval=1739772 TSecr=242957
	 13 1.878737768  192.168.3.5 → 192.168.3.4  TCP 1514 57662→5001 [ACK] Seq=37 Ack=1 Win=29312 Len=1448 TSval=242958 TSecr=1739772
	 14 1.878864382  192.168.3.5 → 192.168.3.4  TCP 1514 57662→5001 [ACK] Seq=1485 Ack=1 Win=29312 Len=1448 TSval=242958 TSecr=1739772
	 15 1.878911439  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=1485 Win=31872 Len=0 TSval=1739773 TSecr=242958
	 16 1.878947086  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=2933 Win=34816 Len=0 TSval=1739773 TSecr=242958
	 17 1.878965295  192.168.3.5 → 192.168.3.4  TCP 1514 57662→5001 [ACK] Seq=2933 Ack=1 Win=29312 Len=1448 TSval=242958 TSecr=1739772
	 18 1.879070765  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=4381 Win=37760 Len=0 TSval=1739773 TSecr=242958
	 19 1.879114564  192.168.3.5 → 192.168.3.4  TCP 1514 57662→5001 [ACK] Seq=4381 Ack=1 Win=29312 Len=1448 TSval=242958 TSecr=1739772
	 20 1.879152654  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=5829 Win=40576 Len=0 TSval=1739773 TSecr=242958
	 21 1.879210738  192.168.3.5 → 192.168.3.4  TCP 1514 57662→5001 [ACK] Seq=5829 Ack=1 Win=29312 Len=1448 TSval=242958 TSecr=1739772
	 22 1.879239534  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=7277 Win=43520 Len=0 TSval=1739773 TSecr=242958
	 23 1.879347950  192.168.3.5 → 192.168.3.4  TCP 1514 57662→5001 [ACK] Seq=7277 Ack=1 Win=29312 Len=1448 TSval=242958 TSecr=1739772
	 24 1.879410092  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=8725 Win=46336 Len=0 TSval=1739773 TSecr=242958
	 25 1.879456342  192.168.3.5 → 192.168.3.4  TCP 1514 57662→5001 [ACK] Seq=8725 Ack=1 Win=29312 Len=1448 TSval=242958 TSecr=1739772
	 26 1.879481497  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=10173 Win=49280 Len=0 TSval=1739773 TSecr=242958
	 27 1.879599278  192.168.3.5 → 192.168.3.4  TCP 1514 57662→5001 [ACK] Seq=10173 Ack=1 Win=29312 Len=1448 TSval=242958 TSecr=1739772
	 28 1.879661774  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=11621 Win=52224 Len=0 TSval=1739773 TSecr=242958
	 29 1.879703379  192.168.3.5 → 192.168.3.4  TCP 1514 57662→5001 [ACK] Seq=11621 Ack=1 Win=29312 Len=1448 TSval=242958 TSecr=1739772
	 30 1.879724914  192.168.3.4 → 192.168.3.5  TCP 66 5001→57662 [ACK] Seq=1 Ack=13069 Win=55040 Len=0 TSval=1739773 TSecr=242958
	30 packets captured



Encontrar los 3 paquetes del handshake de tcp

	Els 3 paquets de handshake es troben a les línies 8, 9 i 10:
	
	 8 1.877854606  192.168.3.5 → 192.168.3.4  TCP 74 57662→5001 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM=1 TSval=242957 TSecr=0 WS=128
	 9 1.877961917  192.168.3.4 → 192.168.3.5  TCP 74 5001→57662 [SYN, ACK] Seq=0 Ack=1 Win=28960 Len=0 MSS=1460 SACK_PERM=1 TSval=1739772 TSecr=242957 WS=128
	 10 1.878161602  192.168.3.5 → 192.168.3.4  TCP 66 57662→5001 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=242957 TSecr=1739772



Abrir dos servidores en dos puertos distintos
	
	# iperf -s -p 5001
	
	[root@j04 ~]# iperf -s -p 5001
	------------------------------------------------------------
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)
	------------------------------------------------------------

	
	
	# iperf -s -p 5002
	
	[isx45128227@j04 ~]$ iperf -s -p 5002
	------------------------------------------------------------
	Server listening on TCP port 5002
	TCP window size: 85.3 KByte (default)
	------------------------------------------------------------



Observar como quedan esos puertos abiertos con netstat.

	# netstat

	[root@j04 netswithlinux]# netstat
	Active Internet connections (w/o servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State      
	tcp        0      0 j04.informatica.e:37624 mad06s25-in-f131.:https ESTABLISHED
	tcp      368      0 j04.informatica.e:37626 mad06s25-in-f131.:https ESTABLISHED
	tcp      368      0 j04.informatica.e:37234 mad06s09-in-f131.:https ESTABLISHED
	tcp      368      0 j04.informatica.e:37628 mad06s25-in-f131.:https ESTABLISHED
	tcp      368      0 j04.informatica.e:37258 mad06s09-in-f131.:https ESTABLISHED
	tcp      368      0 j04.informatica.e:37616 mad06s25-in-f131.:https ESTABLISHED
	tcp        0      0 j04.informatica.e:41506 mad06s09-in-f138.:https ESTABLISHED
	tcp        0      0 j04.informatica.esc:868 gandhi.informatica.:nfs ESTABLISHED
	tcp        0      0 j04.informatica.e:37252 mad06s09-in-f131.:https ESTABLISHED
	tcp        0      0 j04.informatica.e:37256 mad06s09-in-f131.:https ESTABLISHED
	tcp        0      0 j04.informatica.e:52726 mad06s09-in-f14.1:https ESTABLISHED
	tcp      368      0 j04.informatica.e:37622 mad06s25-in-f131.:https ESTABLISHED
	tcp        0      0 j04.informatica.e:58206 gandhi.informatica:ldap ESTABLISHED
	tcp      368      0 j04.informatica.e:37630 mad06s25-in-f131.:https ESTABLISHED
	tcp        0      0 j04.informatica.e:40346 192.0.73.2:https        ESTABLISHED
	tcp      368      0 j04.informatica.e:37236 mad06s09-in-f131.:https ESTABLISHED
	tcp        0      0 j04.informatica.e:34380 mad01s26-in-f4.1e:https ESTABLISHED
	udp6       0      0 localhost:58980         localhost:58980         ESTABLISHED
	Active UNIX domain sockets (w/o servers)
	Proto RefCnt Flags       Type       State         I-Node   Path
	unix  2      [ ]         DGRAM                    16136    /var/run/chrony/chronyd.sock
	unix  2      [ ]         DGRAM                    22055    /run/user/42/systemd/notify
	unix  2      [ ]         DGRAM                    25138    /run/user/202163/systemd/notify
	unix  3      [ ]         DGRAM                    11148    /run/systemd/notify
	unix  36     [ ]         DGRAM                    11156    /run/systemd/journal/dev-log
	unix  7      [ ]         DGRAM                    11165    /run/systemd/journal/socket
	unix  3      [ ]         SEQPACKET  CONNECTED     263708   @0000d
	unix  3      [ ]         STREAM     CONNECTED     220089   




Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

	#Server
	
	iperf -s
	
	#Client
	
	iperf -t 60 -c SERVER_IP


Mientras tanto con netstat mirar conexiones abiertas

	# netstat

	[root@j04 netswithlinux]# netstat




