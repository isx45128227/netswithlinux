
           internet                                         ((-+
        +---------------------------+                          |
                      |DYN_IP_address          +-))            |.101
                      |inet                    |            +-------+
                  +--------+                   |            |tabletA|
                  | Router |eth1  10.0.0.0/24  |.100        +-------+
                  | ISP    +---------------------+
                  |        |.1       |.10     |.11         ((-+
                  +--------+         |        |               |
                      |eth2      +------+ +-------            |.102
   10.10.2.0/24       |.1        |host A| |host B|         +-------+
  --------------------------     +------+ +------+         |tabletB|
           |.10                                            +-------+
           |
    +-------------+
    |server       |
    |www          |
    |ports 80,443 |
    +-------------+


CONFIGURAR UN ROUTER

1. poner IPs
Alberto
ip a a 10.0.0.1/24 dev eth1
ip a a 10.10.2.1/24 dev eth2
dhclient inet



2. Routing
En este caso, como no hay más redes y el default gateway me lo dan por dhcp
no hace falta configurar nada más, excepto el bit de forwarding
 echo 1 > /proc/sys/net/ipv4/ip_forward



3. NAT (REDIRECCIÓN DE PUERTOS Y MASQUERADE)

iptables -t nat -A POSTROUTING -o inet -j MASQUERADE


iptables -t nat -A PREROUTING -i inet -p tcp --dport 80  -j DNAT --to 10.10.2.10:80
iptables -t nat -A PREROUTING -i inet -p tcp --dport 443  -j DNAT --to 10.10.2.10:443



4. Filtrado

iptables -t filter -A FORWARD  -d 10.10.2.10  -s 10.0.0.101 -p tcp --dport 80 -j ACCEPT 
iptables -t filter -A FORWARD  -d 10.10.2.10  -s 10.0.0.101 -p tcp --dport 443 -j ACCEPT 
iptables -t filter -A FORWARD  -d 10.10.2.10  -s 10.0.0.102 -p tcp --dport 80 -j ACCEPT
iptables -t filter -A FORWARD  -d 10.10.2.10  -s 10.0.0.102 -p tcp --dport 443 -j ACCEPT
iptables -t filter -A FORWARD  -s 10.0.0.101 -o eth2 -j DROP
iptables -t filter -A FORWARD  -s 10.0.0.102 -o eth2 -j DROP



#REDUCIR LAS TABLAS

supernetting:
    
iptables -t filter -A FORWARD  -d 10.10.2.10  -s 10.0.0.100/30 -p tcp --dport 80 -j ACCEPT 
iptables -t filter -A FORWARD  -d 10.10.2.10  -s 10.0.0.100/30 -p tcp --dport 443 -j ACCEPT 
iptables -t filter -A FORWARD  -s 10.0.0.100/30  -j DROP


iptables -t filter -A FORWARD -d 10.10.2.10 \
							  -p tcp		\
							  -m iprange --src-range 10.0.0.101-10.0.0.102 \ 
							  -m multiport --destination-ports 80,443  \
							  -j ACCEPT


iptables -t filter -A FORWARD -m iprange --src-range 10.0.0.101-10.0.0.102
							  -o eth2
							  -j DROP








5. QoS


Buscar un modulo de iptables que permita filtrar rangos IP

man iptables-extensions

- iptables -m iprange |–src-range IP1-IP2 -j ACTION (Direccion origen)
                 |-dst-range IP1-IP2 -j ACTION (Direccion Destino)
                                          
                  
Buscar un modulo de iptables que permita filtrar por MAC
- iptables -m mac --mac-source aa:aa:aa:aa:aa:a -j ACTION


Buscar un modulo de iptables que permita filtrar por franja horaria
- iptables -m time --timestart HORA1 --timestop HORA2 -j ACTION (Formato 24h HH:MM)


Buscar un módulo para permitir rangos de puertos en el filtrado, en una sóla regla varios puertos

iptables -m multiport 	 --source-ports,--sports port[,port|,port:port]...
						 --destination-ports,--dports port[,port|,port:port]...
						 --ports port[,port|,port:port]...
         




