EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# adreça a la interface



# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  



# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# ports independents



# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# canviem nom perquè quedi més clar

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes



# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

##### RESETEJAR ROUTER

		[roger@j04 netswithlinux]$ telnet 192.168.88.1
		Trying 192.168.88.1...
		Connected to 192.168.88.1.
		Escape character is '^]'.

		MikroTik v6.33.5 (stable)
		Login: admin
		Password: 


		  MMM      MMM       KKK                          TTTTTTTTTTT      KKK
		  MMMM    MMMM       KKK                          TTTTTTTTTTT      KKK
		   MM MMMM MMM  III  KKK  KKK  RRRRRR     OOOOOO      TTT     III  KKK  KKK       MMM  MM  MMM  III  KKKKK     RRR  RRR  OOO  OOO     TTT     III  KKKKK



		  MikroTik RouterOS 6.33.5 (c) 1999-2015       http://www.mikrotik.com/

		[?]             Gives the list of available commands
		command [?]     Gives help on the command and list of arguments

		[Tab]           Completes the command/word. If the input is ambiguous,
						a second [Tab] gives possible options

		/               Move up to base level
		..              Move up one level
		/command        Use command at the base level
		jan/02/1970 00:00:16 system,error,critical router was rebooted without proper shu
		tdown
		jan/02/1970 00:00:17 system,error,critical router was rebooted without proper shu
		tdown
		  
		[admin@infMKT04] > /system reset-configuration 
		Dangerous! Reset anyway? [y/N]: 
		y
		system configuration will be reset
		Connection closed by foreign host.

		[roger@j04 netswithlinux]$ ip a a 192.168.88.4/24 dev enp0s18f1u3


##### AFEGIR CONFIGURACIÓ

		[admin@MikroTik] > # Cambiamos direccin IP para que la gestione directamente la interface 2
		[admin@MikroTik] > ip address set 0 interface=ether2-master
		[admin@MikroTik] > 
		[admin@MikroTik] > # Eliminamos el bridge
		[admin@MikroTik] > 
		[admin@MikroTik] > /interface bridge port remove 1
		[admin@MikroTik] > /interface bridge port remove 0
		[admin@MikroTik] > /interface bridge remove 0  
		[admin@MikroTik] > 
		[admin@MikroTik] > # Eliminamos que un puerto sea master de otro
		[admin@MikroTik] > 
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether3 ] master-port=none
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether4 ] master-port=none
		[admin@MikroTik] > 
		[admin@MikroTik] > # Cambiamos nombres a los puertos
		[admin@MikroTik] > 
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether1 ] name=eth1
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether2 ] name=eth2
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether3 ] name=eth3
		[admin@MikroTik] > /interface ethernet set [ find default-name=ether4 ] name=eth4
		[admin@MikroTik] > 
		[admin@MikroTik] > # Deshabilitamos la wifi
		[admin@MikroTik] > 
		[admin@MikroTik] > /interface wireless set [ find default-name=wlan1 ] disabled=yes
		[admin@MikroTik] > 
		[admin@MikroTik] > # Eliminamos servidor y cliente dhcp
		[admin@MikroTik] > 
		[admin@MikroTik] > /ip pool remove 0
		[admin@MikroTik] > /ip dhcp-server network remove 0
		[admin@MikroTik] > /ip dhcp-server remove 0
		[admin@MikroTik] > /ip dhcp-client remove 0
		[admin@MikroTik] > /ip dns static remove 0
		[admin@MikroTik] > /ip dns set allow-remote-requests=no
		[admin@MikroTik] > 
		[admin@MikroTik] > # Eliminamos regla de nat masquerade
		[admin@MikroTik] > /ip firewall nat remove 0

		[admin@MikroTik] > /system identity set name=mkt04             
		[admin@mkt04] > /ip firewall nat remove 0  


Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt00
/system backup save name="20170317_zeroconf"
```


##### EXERCICIS


###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router
	
	Per resetejar el router caldrà fer:
		[admin@mkt04] > /system reset-configuration 
		Dangerous! Reset anyway? [y/N]: 
		y
		system configuration will be reset
		Connection closed by foreign host.
	
	o mitjançant el botó físic reset del router.
	
	Per tal de no perdre tota la informació que conté el nostre router, caldrà fer un backup de la configuració abans de fer el reset.
		[admin@mkt04] > /system backup save name="20170317_zeroconf"
		[admin@mkt04] > /file print
		 # NAME                                                 TYPE                                                      SIZE CREATION-TIME       
		 0 skins                                                directory                                                      jan/01/1970 00:00:01
		 1 auto-before-reset.backup                             backup                                                 23.6KiB jan/02/1970 00:07:17
		 2 test.backup                                          backup                                                 19.8KiB jan/02/1970 01:33:58
		 3 20170317_zeroconf.backup                             backup                                                 23.6KiB jan/02/1970 00:06:21

	
	Un cop fet això resetegem el router com s'ha indicat anteriorment i procedim a la càrrega del backup (comprovant primer que no queda cap configuració inicial).
	
		[admin@mkt04] > /system reset-configuration 
		Dangerous! Reset anyway? [y/N]: 
		y
		system configuration will be reset
		Connection closed by foreign host.
		
		[admin@MikroTik] > /export
		# jan/02/1970 00:00:48 by RouterOS 6.33.5
		# software id = 3GAD-YVGP
		#
		/interface bridge
		add admin-mac=6C:3B:6B:C3:89:F9 auto-mac=no comment=defconf name=bridge
		/interface wireless
		set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce disabled=no distance=indoors frequency=auto mode=ap-bridge \
			ssid=MikroTik-C389FC wireless-protocol=802.11
		/interface ethernet
		set [ find default-name=ether2 ] name=ether2-master
		set [ find default-name=ether3 ] master-port=ether2-master
		set [ find default-name=ether4 ] master-port=ether2-master
		/ip neighbor discovery
		set ether1 discover=no
		set bridge comment=defconf
		/ip pool
		add name=default-dhcp ranges=192.168.88.10-192.168.88.254
		/ip dhcp-server
		add address-pool=default-dhcp disabled=no interface=bridge name=defconf
		/interface bridge port
		add bridge=bridge comment=defconf interface=ether2-master
		add bridge=bridge comment=defconf interface=wlan1
		/ip address
		add address=192.168.88.1/24 comment=defconf interface=bridge network=192.168.88.0
		/ip dhcp-client
		add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
		/ip dhcp-server network
		add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
		/ip dns
		set allow-remote-requests=yes
		/ip dns static
		add address=192.168.88.1 name=router
		/ip firewall filter
		add chain=input comment="defconf: accept ICMP" protocol=icmp
		add chain=input comment="defconf: accept establieshed,related" connection-state=established,related
		add action=drop chain=input comment="defconf: drop all from WAN" in-interface=ether1
		add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related
		add chain=forward comment="defconf: accept established,related" connection-state=established,related
		add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
		add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new \
			in-interface=ether1
		/ip firewall nat
		add action=masquerade chain=srcnat comment="defconf: masquerade" out-interface=ether1
		/system routerboard settings
		set cpu-frequency=650MHz protected-routerboot=disabled
		/tool mac-server
		set [ find default=yes ] disabled=yes
		add interface=bridge
		/tool mac-server mac-winbox
		set [ find default=yes ] disabled=yes
		add interface=bridge

	Com es pot veure no es manté la configuració que havíem carregat, per tant carreguem el backup de la següent manera.
		
		[admin@MikroTik] > system backup load name="20170317_zeroconf"
		password: 
		Restore and reboot? [y/N]: 
		y
		Restoring system configuration
		System configuration restored, rebooting now
		Connection closed by foreign host.
	
	Connectem de nou amb el router i comprovem que les configuracions són correctes.

		[admin@mkt04] > /export 
		# jan/02/1970 00:01:02 by RouterOS 6.33.5
		# software id = 3GAD-YVGP
		#
		/interface wireless
		set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce distance=indoors frequency=auto mode=ap-bridge ssid=\
			MikroTik-C389FC wireless-protocol=802.11
		/interface ethernet
		set [ find default-name=ether1 ] name=eth1
		set [ find default-name=ether2 ] name=eth2
		set [ find default-name=ether3 ] name=eth3
		set [ find default-name=ether4 ] name=eth4
		/ip neighbor discovery
		set eth1 discover=no
		/interface wireless security-profiles
		set [ find default=yes ] supplicant-identity=MikroTik
		/ip address
		add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
		/ip firewall filter
		add chain=input comment="defconf: accept ICMP" protocol=icmp
		add chain=input comment="defconf: accept establieshed,related" connection-state=established,related
		add action=drop chain=input comment="defconf: drop all from WAN" in-interface=eth1
		add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related
		add chain=forward comment="defconf: accept established,related" connection-state=established,related
		add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
		add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new \
			in-interface=eth1
		/system identity
		set name=mkt04
		/system routerboard settings
		set cpu-frequency=650MHz protected-routerboot=disabled
		/tool mac-server
		set [ find default=yes ] disabled=yes
		add
		/tool mac-server mac-winbox
		set [ find default=yes ] disabled=yes
		add



### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*1XX: red pública
*2XX: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 ssid="free104"
/interface wireless add ssid="private204" master-interface=wlan1
```
Afegim les interfícies virtuals wifi addicionals i canviar els ssid:
	[admin@mkt04] > /interface wireless set [find name=wlan1] ssid="free104"
	[admin@mkt04] > /interface wireless add ssid="private204" master-interface=wlan1
	[admin@mkt04] > /interface wireless print               
	Flags: X - disabled, R - running 
	 0 X  name="wlan1" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:FC arp=enabled interface-type=Atheros AR9300 mode=ap-bridge 
		  ssid="free104" frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag 
		  vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes 
		  default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 

	 1 X  name="wlan2" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C3:89:FC arp=enabled interface-type=virtual-AP master-interface=wlan1 
		  ssid="private204" vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled 
		  default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no 
		  security-profile=default 

	

###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estas interfaces. Cámbiales
el nombre a wFree wPrivate y dejalas deshabilitadas

	Les interíficies wifi apareixen com a deshabilitades perquè per defecte estan deshabilitades (segons informació del manual).
	
	Per habilitar-les caldrà fer:
	
	[admin@mkt04] > /interface wireless set [find ssid="free104"] disabled=no       
	[admin@mkt04] > /interface wireless set [find ssid="private204"] disabled=no
	[admin@mkt04] > /interface wireless print 
	Flags: X - disabled, R - running 
	 0    name="wlan1" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:FC arp=enabled interface-type=Atheros AR9300 mode=ap-bridge 
		  ssid="free104" frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag 
		  vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes 
		  default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 

	 1    name="wlan2" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C3:89:FC arp=enabled interface-type=virtual-AP master-interface=wlan1 
		  ssid="private204" vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled 
		  default-authentication=yes default

	Per deshabilitar-les caldrà fer:
	
	[admin@mkt04] > /interface wireless set [find ssid="free104"] disabled=yes
	[admin@mkt04] > /interface wireless set [find ssid="private204"] disabled=yes
	
	Canviem el nom de les interfícies:
	
	[admin@mkt04] > /interface wireless set [find ssid="free104"] ssid="wFree"
	[admin@mkt04] > /interface wireless set [find ssid="private204"] ssid="wPrivate"
	[admin@mkt04] > /interface wireless print                                  
	Flags: X - disabled, R - running 
	 0 X  name="wlan1" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:FC arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="wFree" 
		  frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 
		  wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes 
		  default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 

	 1 X  name="wlan2" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C3:89:FC arp=enabled interface-type=virtual-AP master-interface=wlan1 
		  ssid="wPrivate" vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled 
		  default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no 
		  security-profile=default 
	
	
	
## Crear interfaces virtuales y hacer bridges

```
/interface vlan add name eth4-vlan1XX vlan-id=1XX interface=eth4
/interface vlan add name eth4-vlan2XX vlan-id=2XX interface=eth4   

/interface bridge add name=br-vlan1XX
/interface bridge add name=br-vlan2XX


/interface bridge port add interface=eth4-vlan1XX bridge=br-vlan1XX
/interface bridge port add interface=eth3 bridge=br-vlan1XX
/interface bridge port add interface=wFree  bridge=br-vlan1XX      


/interface bridge port add interface=eth4-vlan2XX bridge=br-vlan2XX
/interface bridge port add interface=wPrivate   bridge=br-vlan2XX

/interface print

```







### [ejercicio3] Comenta cada una de estas líneas de la configuración
anterior para que quede bien documentado


### [ejercicio4] Pon una ip 172.17.1XX.1/24 a br-vlan1XX 
y 172.17.2XX.1/24 a br-vlan2XX y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración

### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 

### [ejercicio6] Activar redes wifi y dar seguridad wpa2

### [ejercicio6b] Opcional (monar portal cautivo)

### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.

### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado

